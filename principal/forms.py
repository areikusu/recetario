__author__ = 'Aleix'

# encoding:utf-8
from django.forms import ModelForm
from django import forms
from principal.models import Recipe, Comment


class ContactForm(forms.Form):
    mail = forms.EmailField(label='Tu email')
    message = forms.CharField(widget=forms.Textarea)


class RecipeForm(ModelForm):
    class Meta:
        model = Recipe


class CommentForm(ModelForm):
    class Meta:
        model = Comment