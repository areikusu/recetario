# encoding:utf-8
from tastypie.utils.timezone import now
from django.db import models
from django.contrib.auth.models import User
from django.contrib.sites.models import Site

# Create your models here.

class Recipe(models.Model):
    tittle = models.CharField(max_length=100, unique=True)
    ingredients = models.TextField(help_text='Redacta los ingredientes')
    preparation = models.TextField(verbose_name='Preparación')
    image = models.ImageField(upload_to='recetas', verbose_name='Imágen')
    register_time = models.DateTimeField(default=now())
    user = models.ForeignKey(User)

    def get_image_url(self):
        if self.image and hasattr(self.image, 'url'):
            return self.image.url
        else:
            return '/media/recetas/Leprechaun.png'

    def __str__(self):
        return self.tittle


class Comment(models.Model):
    recipe = models.ForeignKey(Recipe)
    text = models.TextField(help_text='Your comment', verbose_name='Comment')

    def __str__(self):
        return self.text