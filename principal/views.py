from principal.models import Recipe, Comment
from django.contrib.auth.models import User
from principal.forms import ContactForm, RecipeForm, CommentForm
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.core.mail import EmailMessage

from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required


def sobre(request):
    html = "<html><body><b>Mi first Django Unchained Project</b></body></html>"
    return HttpResponse(html)


def inicio(request):
    recipes = Recipe.objects.all()
    return render_to_response('inicio.html', {'recipes': recipes},
                              context_instance=RequestContext(request))


def users(request):
    users_list = User.objects.all()
    recipes = Recipe.objects.all()
    return render_to_response('usuarios.html', {'users': users_list, 'recipes': recipes},
                              context_instance=RequestContext(request))


def recipes_list(request):
    recipes = Recipe.objects.all()
    return render_to_response('recetas.html', {'datos': recipes},
                              context_instance=RequestContext(request))


def recipe_detail(request, id_recipe):
    dato = get_object_or_404(Recipe, pk=id_recipe)
    comments = Comment.objects.filter(recipe=dato)
    return render_to_response('receta.html', {'recipe': dato, 'comments': comments},
                              context_instance=RequestContext(request))


def contact(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            title = 'Mensaje desde Super Recetas'
            content = form.cleaned_data['message'] + "\n"
            content += 'Comunicar a: ' + form.cleaned_data['mail']
            mail = EmailMessage(title, content, to=['aleix.casanova@gmail.com'])
            mail.send()
            return HttpResponseRedirect('/')
    else:
         form = ContactForm()
    return render_to_response('contactform.html', {'form':form},
                              context_instance=RequestContext(request))


def new_recipe(request):
    if request.method == 'POST':
        form = RecipeForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/recipes')
    else:
        form = RecipeForm()
    return render_to_response('recipeform.html', {'form': form},
                              context_instance=RequestContext(request))


def new_comment(request):
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/recipes')
    else:
        form = CommentForm()
    return render_to_response('commentform.html', {'form': form},
                              context_instance=RequestContext(request))


def new_user(request):
    if request.method == 'POST':
        uform = UserCreationForm(request.POST)
        if uform.is_valid:
            uform.save()
            return HttpResponseRedirect('/')
    else:
        uform = UserCreationForm()
    return render_to_response('newuser.html', {'form': uform},
                              context_instance=RequestContext(request))


def sign_in(request):
    if not request.user.is_anonymous():
        return HttpResponseRedirect('/private')
    if request.method == 'POST':
        uform = AuthenticationForm(request.POST)
        if uform.is_valid:
            user = request.POST['username']
            passkey = request.POST['password']
            access = authenticate(username=user, password=passkey)
            if access is not None:
                if access.is_active:
                    login(request, access)
                    return HttpResponseRedirect('/private')
                else:
                    return render_to_response('unactive.html', context_instance=RequestContext(request))
            else:
                return render_to_response('nouser.html', context_instance=RequestContext(request))
    else:
        uform = AuthenticationForm()
    return render_to_response('signin.html',{'form': uform}, context_instance=RequestContext(request))



@login_required(login_url='/signin')
def private(request):
    tuser = request.user
    return render_to_response('private.html', {'user': tuser}, context_instance=RequestContext(request))


@login_required(login_url='/signin')
def close_session(request):
    logout(request)
    return HttpResponseRedirect('/')