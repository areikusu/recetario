# api.py
__author__ = 'Aleix'
from tastypie.authorization import DjangoAuthorization
from tastypie import fields
from tastypie.resources import ModelResource
from principal.models import User, Recipe, Comment
from authentication import OAuth20Authentication
from tastypie.authorization import Authorization


class FilterAuthorizedObjects(Authorization):
    def create_list(self, object_list, bundle):
        return object_list

    def read_list(self, object_list, bundle):

        return object_list.filter(user=bundle.request.user)


class UserResource(ModelResource):
    class Meta:
        queryset = User.objects.all()
        resource_name = 'user'
        fields = ['username', 'first_name', 'last_name', 'last_login', 'id']
        allowed_methods = ['get']
        authorization = DjangoAuthorization()
        authentication = OAuth20Authentication()


class RecipeResource(ModelResource):
    user = fields.ForeignKey(UserResource, 'user')

    class Meta:
        queryset = Recipe.objects.all()
        resource_name = 'recipe'
        authorization = FilterAuthorizedObjects()
        authentication = OAuth20Authentication()


