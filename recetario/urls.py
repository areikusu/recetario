from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from tastypie.api import Api
from principal.api import RecipeResource, UserResource

admin.autodiscover()

v1_api = Api(api_name='v1')
v1_api.register(UserResource())
v1_api.register(RecipeResource())

urlpatterns = patterns('',
                       url(r'^$', 'principal.views.inicio', name='home'),
                       url(r'^o/', include('provider.oauth2.urls', namespace='oauth2')),
                       url(r'^api/', include(v1_api.urls)),
                       url(r'^favit/', include('favit.urls')),
                       url(r'^accounts/login/$', 'django.contrib.auth.views.login', {'template_name': 'signin.html'}),
                       url(r'^sobre/$', 'principal.views.sobre'),
                       url(r'^user/new/$', 'principal.views.new_user'),
                       url(r'^users/$', 'principal.views.users'),
                       url(r'^signin/$', 'principal.views.sign_in'),
                       url(r'^private/$', 'principal.views.private'),
                       url(r'^recipes/newrecipe/$', 'principal.views.new_recipe'),
                       url(r'close/$', 'principal.views.close_session'),
                       url(r'^recipes/$', 'principal.views.recipes_list'),
                       url(r'^comments/$', 'principal.views.new_comment'),
                       url(r'^recipes/recipe/(?P<id_recipe>\d+)$', 'principal.views.recipe_detail'),
                       url(r'^contact/$', 'principal.views.contact'),
                       url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
                           {'document_root': settings.MEDIA_ROOT, }
                           ),
                       )
